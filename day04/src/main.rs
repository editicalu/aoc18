use regex;

#[derive(Eq, PartialEq, PartialOrd, Clone, Debug)]
struct Time(usize, usize, usize, usize, usize);

impl Ord for Time {
    fn cmp(&self, other: &Time) -> std::cmp::Ordering {
        self.0
            .cmp(&other.0)
            .then(self.1.cmp(&other.1))
            .then(self.2.cmp(&other.2))
            .then(self.3.cmp(&other.3))
            .then(self.4.cmp(&other.4))
    }
}

#[derive(Eq, PartialEq, PartialOrd, Clone, Debug)]
enum Entry {
    WakesUp,
    Begins(usize),
    FallsAsleep,
}

#[derive(Eq, PartialEq, PartialOrd, Clone, Debug)]
struct LogEntry {
    pub time: Time,
    pub entry: Entry,
}

impl Ord for LogEntry {
    fn cmp(&self, other: &LogEntry) -> std::cmp::Ordering {
        self.time.cmp(&other.time)
    }
}

fn main() {
    use std::collections::HashMap;
    use std::io::Read;

    let mut data = std::fs::File::open("input").unwrap();
    let mut buf = String::new();
    data.read_to_string(&mut buf).unwrap();

    let regex = regex::Regex::new(r"\[(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2})\] (wakes up|falls asleep|Guard #(\d+) begins shift)").unwrap();

    let input: Vec<LogEntry> = buf
        .lines()
        .map(|line| regex.captures(line).expect("Invalid input"))
        .map(|matches| {
            let time = Time(
                matches[1].parse().unwrap(),
                matches[2].parse().unwrap(),
                matches[3].parse().unwrap(),
                matches[4].parse().unwrap(),
                matches[5].parse().unwrap(),
            );

            if let Some(guard) = matches.get(7) {
                let guard: usize = guard.as_str().parse().unwrap();
                LogEntry {
                    time,
                    entry: Entry::Begins(guard),
                }
            } else if &matches[6] == "falls asleep" {
                LogEntry {
                    time,
                    entry: Entry::FallsAsleep,
                }
            } else if &matches[6] == "wakes up" {
                LogEntry {
                    time,
                    entry: Entry::WakesUp,
                }
            } else {
                panic!("{}", &matches[6]);
            }
        })
        .collect();

    let mut input_sorted = input.clone();
    input_sorted.sort_unstable();
    let input_sorted = input_sorted;

    // Minutes per guard
    let mut map: HashMap<usize, usize> = HashMap::new();
    let mut sleeping = 0;
    let mut guard = 0;
    for ref log_entry in input_sorted.iter() {
        match log_entry.entry {
            Entry::Begins(new_guard) => guard = new_guard,
            Entry::FallsAsleep => sleeping = log_entry.time.4,
            Entry::WakesUp => {
                let sleeped = log_entry.time.4 - sleeping;
                if let Some(sum) = map.get(&guard) {
                    map.insert(guard, sum + sleeped);
                } else {
                    map.insert(guard, sleeped);
                }
            }
        }
    }

    // Find the guard which sleeps the most
    let mut max_guard = 0;
    let mut max_sleep = 0;
    for (&key, &value) in map.iter() {
        if value > max_sleep {
            max_guard = key;
            max_sleep = value;
        }
    }

    // Count per minute per guard
    let mut detailled_map: HashMap<usize, [usize; 60]> = HashMap::new();
    map.keys().for_each(|&key| {
        detailled_map.insert(key, [0; 60]);
    });

    // Guards sleeping per minute
    let mut active_guard = 0;
    let mut started_sleeping = 0;
    for log_entry in input_sorted.iter() {
        match log_entry.entry {
            Entry::WakesUp => {
                let counters = detailled_map.get_mut(&active_guard).unwrap();
                for minute in started_sleeping..(log_entry.time.4) {
                    counters[minute] += 1;
                }
            }
            Entry::Begins(guard) => active_guard = guard,
            Entry::FallsAsleep => {
                started_sleeping = log_entry.time.4;
            }
        }
    }

    // Find the most sleeping guard
    let mut max = std::usize::MIN;
    let mut minute = 0;
    for entry in detailled_map
        .get_mut(&max_guard)
        .unwrap()
        .iter()
        .enumerate()
    {
        if max < *entry.1 {
            minute = entry.0;
            max = *entry.1;
        }
    }

    println!("A: {}", max_guard * minute);

    // Find the guard most sleeping in any minute
    let mut max: (usize, usize) = (0, 0);
    let mut max_amount = 0;
    for &guard in detailled_map.keys() {
        let counters = detailled_map.get(&guard).unwrap();
        for (minute, &amount) in counters.iter().enumerate() {
            if amount > max_amount {
                max = (guard, minute);
                max_amount = amount;
            }
        }
    }

    println!("B: {}", max.0 * max.1);
}
