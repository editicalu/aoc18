use regex::Regex;
use std::collections::HashMap;

struct Entry {
    id: usize,
    begin_x: usize,
    begin_y: usize,
    x_delta: usize,
    y_delta: usize,
}

fn put_on_map(entry: &Entry, map: &mut HashMap<(usize, usize), usize>) {
    let begin_x = entry.begin_x;
    let begin_y = entry.begin_y;
    let x_delta = entry.x_delta;
    let y_delta = entry.y_delta;

    for x in begin_x..(begin_x + x_delta) {
        for y in begin_y..(begin_y + y_delta) {
            if let Some(item) = map.get(&(x, y)) {
                map.insert((x, y), *item + 1);
            } else {
                map.insert((x, y), 1);
            }
        }
    }
}

fn no_overlap(entry: &Entry, map: &HashMap<(usize, usize), usize>) -> bool {
    let begin_x = entry.begin_x;
    let begin_y = entry.begin_y;
    let x_delta = entry.x_delta;
    let y_delta = entry.y_delta;

    for x in begin_x..(begin_x + x_delta) {
        for y in begin_y..(begin_y + y_delta) {
            if map.get(&(x, y)).unwrap() != &1 {
                return false;
            }
        }
    }

    true
}

fn main() {
    use std::io::Read;
    let mut file = std::fs::File::open("input")
        .expect("Could not open the file called `input` in the current folder");

    let mut data = String::with_capacity(30000);
    file.read_to_string(&mut data).unwrap();

    let regex = Regex::new(r"#(\d+) @ (\d+),(\d+): (\d+)x(\d+)").unwrap();

    let input: Vec<Entry> = data
        .lines()
        .map(|string| regex.captures(string).unwrap())
        .map(|mtch| Entry {
            id: mtch[1].parse().unwrap(),
            begin_x: mtch[2].parse().unwrap(),
            begin_y: mtch[3].parse().unwrap(),
            x_delta: mtch[4].parse().unwrap(),
            y_delta: mtch[5].parse().unwrap(),
        })
        .collect();

    let mut map: HashMap<(usize, usize), usize> = HashMap::new();
    for entry in input.iter() {
        put_on_map(&entry, &mut map);
    }

    println!("A: {}", map.values().filter(|&&entry| entry >= 2).count());

    for entry in input.iter() {
        if no_overlap(&entry, &map) {
            println!("B: {}", entry.id);
            break;
        }
    }
}
