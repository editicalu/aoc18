#![feature(type_ascription)]

use std::collections::HashSet;
use std::io::Read;

fn main() {
    let mut data = {
        let file = std::fs::File::open("./input");
        match file {
            Ok(file) => file,
            Err(_) => panic!(),
        }
    };

    let mut buf = Vec::new();
    data.read_to_end(&mut buf).unwrap();

    let buf = String::from_utf8(buf).unwrap();
    let numbers: Vec<isize> = buf
        .split_whitespace()
        .map(|item| item.parse().unwrap())
        .collect();

    let total: isize = numbers.iter().sum();

    println!("A: {}", total);

    let mut freq = 0;
    let mut reached: HashSet<isize> = HashSet::with_capacity(1024);
    reached.insert(0);
    'bloop: loop {
        for change in &numbers {
            freq += change;
            if reached.replace(freq).is_some() {
                break 'bloop;
            };
        }
    }

    println!("B: {}", freq);
}
