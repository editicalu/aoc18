fn count_letters(word: &str) -> [usize; 26] {
    let mut buckets: [usize; 26] = [0; 26];
    for letter in word.chars() {
        buckets[letter as usize - 'a' as usize] += 1;
    }
    buckets
}

/// Returns a string if the difference is exactly one.
fn compare_words(word1: &str, word2: &str) -> Option<String> {
    let iter = word1.chars().zip(word2.chars());
    let mut s = String::with_capacity(10);
    let mut diff = 0;
    for (char1, char2) in iter {
        if char1 == char2 {
            s.push(char1);
        } else {
            diff += 1;
            if diff > 1 {
                return None;
            }
        }
    }
    Some(s)
}

use std::io::Read;

fn main() {
    let mut data = {
        let file = std::fs::File::open("./input");
        match file {
            Ok(file) => file,
            Err(_) => panic!(),
        }
    };

    let mut buf = Vec::new();
    data.read_to_end(&mut buf).unwrap();

    let data = String::from_utf8(buf).unwrap();

    let words: Vec<String> = data
        .split_whitespace()
        .map(|word| word.to_lowercase())
        .collect();

    let mut twos = 0;
    let mut threes = 0;
    for word in &words {
        let buckets = count_letters(word);
        if buckets.contains(&2) {
            twos += 1;
        }
        if buckets.contains(&3) {
            threes += 1;
        }
    }

    let a = twos * threes;
    println!("A: {}", a);

    let ewords: Vec<(usize, &String)> = words.iter().enumerate().collect();

    'bloop: for &(index1, ref word1) in ewords.iter() {
        for &(_, ref word2) in ewords.iter().skip(index1 + 1) {
            if word1 == word2 {
                continue;
            }

            if let Some(common) = compare_words(&word1, &word2) {
                println!("B: {}", common);
                break 'bloop;
            }
        }
    }
}
